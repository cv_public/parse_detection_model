from swin import SwinTransformer
from torch.optim import Adam
from torchmetrics.classification import Accuracy, Recall, MulticlassF1Score
import torch
import pytorch_lightning as pl
import torch.nn as nn


class SwinTransformerPL(pl.LightningModule):
    def __init__(self, model_config):
        super(SwinTransformerPL, self).__init__()
        print("config: ", model_config)
        self.model = SwinTransformer(**model_config)
        self.loss_fn = nn.CrossEntropyLoss()

        self.train_accuracy = Accuracy("multiclass", num_classes=model_config['num_classes'])
        self.val_accuracy = Accuracy("multiclass", num_classes=model_config['num_classes'])
        self.test_accuracy = Accuracy("multiclass", num_classes=model_config['num_classes'])

        self.train_recall = Recall("multiclass", num_classes=model_config['num_classes'])
        self.val_recall = Recall("multiclass", num_classes=model_config['num_classes'])
        self.test_recall = Recall("multiclass", num_classes=model_config['num_classes'])

        self.train_f1 = MulticlassF1Score(num_classes=model_config['num_classes'])
        self.val_f1 = MulticlassF1Score(num_classes=model_config['num_classes'])
        self.test_f1 = MulticlassF1Score(num_classes=model_config['num_classes'])

        self.training_step_outputs = []
        self.validation_step_outputs = []
        self.test_step_outputs = []

    def forward(self, x):
        return self.model(x)

    def configure_optimizers(self):
        optimizer = Adam(self.parameters(), lr=1e-4)
        return optimizer

    def training_step(self, batch, batch_idx):
        inputs, labels = batch
        outputs = self.model(inputs)
        loss = self.loss_fn(outputs, labels)
        self.train_accuracy(outputs, labels)
        self.train_recall(outputs, labels)
        self.train_f1(outputs, labels)
        self.training_step_outputs.append(loss)
        return loss

    def on_train_epoch_end(self):
        train_acc = self.train_accuracy.compute()
        train_recall = self.train_recall.compute()
        train_f1 = self.train_f1.compute()
        epoch_average = torch.stack(self.training_step_outputs).mean()

        self.log('train_accuracy', train_acc, on_epoch=True)
        self.log('train_recall', train_recall, on_epoch=True)
        self.log('train_f1', train_f1, on_epoch=True)
        self.log("training_epoch_average", epoch_average)

        self.training_step_outputs.clear()

    def validation_step(self, batch, batch_idx):
        inputs, labels = batch
        outputs = self.model(inputs)
        loss = self.loss_fn(outputs, labels)
        self.val_accuracy(outputs, labels)
        self.val_recall(outputs, labels)
        self.val_f1(outputs, labels)
        # self.log('val_loss', loss, on_epoch=True)
        self.validation_step_outputs.append(loss)
        return loss

    def on_validation_epoch_end(self):
        val_acc = self.val_accuracy.compute()
        val_recall = self.val_recall.compute()
        val_f1 = self.val_f1.compute()
        epoch_average = torch.stack(self.validation_step_outputs).mean()

        self.log('val_accuracy', val_acc, on_epoch=True)
        self.log('val_recall', val_recall, on_epoch=True)
        self.log('val_f1', val_f1, on_epoch=True)
        self.log("training_epoch_average", epoch_average)

        self.validation_step_outputs.clear()

    def test_step(self, batch, batch_idx):
        inputs, labels = batch
        outputs = self.model(inputs)
        loss = self.loss_fn(outputs, labels)
        self.test_accuracy(outputs, labels)
        self.test_recall(outputs, labels)
        self.test_f1(outputs, labels)
        # self.log('test_loss', loss, on_epoch=True)
        self.test_step_outputs.append(loss)
        return loss

    def on_test_epoch_end(self):
        test_acc = self.test_accuracy.compute()
        test_recall = self.test_recall.compute()
        test_f1 = self.test_f1.compute()
        epoch_average = torch.stack(self.test_step_outputs).mean()

        self.log('test_accuracy', test_acc, on_epoch=True)
        self.log('test_recall', test_recall, on_epoch=True)
        self.log('test_f1', test_f1, on_epoch=True)
        self.log("test_epoch_average", epoch_average)

        self.test_step_outputs.clear()
