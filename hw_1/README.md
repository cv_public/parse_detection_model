# Мультиклассификатор на основе архитектуры SWIN

ФИО: Сторонкин Даниил Андреевич

Предмет: Обработка и генерация изображений

Датасет: https://www.kaggle.com/datasets/fantacher/neu-metal-surface-defects-data/ (classify metal defect)

## Домашнее задание 1

Классы - rolled-in scale, patches, crazing, pitted surface, inclusion, scratches (Sc)

### Пример изображений
<img title="a title" alt="Alt text" src="./docs/In_3.bmp">
<img title="a title" alt="Alt text" src="./docs/Sc_13.bmp">

### Обучение модели

<img title="a title" alt="Alt text" src="./docs/train.png">
<img title="a title" alt="Alt text" src="./docs/val.png">
<img title="a title" alt="Alt text" src="./docs/test.png">

### Эксперимент

Цель эксперимента - получить безлайн для последующих домашних заданий

Идея эксперимента - использовать архитектуру которая пришла из NLP в CV

Вывод - метрики и лосс вышли на плато на 25 эпохе. Результаты на тестовой выборке - F1 - 0.901, Recall - 0.903, Accuracy - 0.903

