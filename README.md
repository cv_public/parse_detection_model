# Мультиклассификатор на основе архитектуры SWIN

ФИО: Сторонкин Даниил Андреевич

Предмет: Обработка и генерация изображений/Глубокие ГАНы

Датасет: https://www.kaggle.com/datasets/fantacher/neu-metal-surface-defects-data/ (classify metal defect)

## ДЗ1 - папка hw_1

## Глубокие ГАНы ДЗ1 - папка deep_hw_1